#!/bin/bash

set -e

echo "------------------------------------------------------"
echo ""
echo " Dependencies"
echo ""
echo "------------------------------------------------------"

apt-get update

apt-get --no-install-recommends install -y \
    curl \
    jq \
    wget \
    sshpass \
    unzip \
    shellcheck \
    bats \
    ca-certificates \
    zip \
    yamllint \
    python3 \
    python3-pip \
    git

#SonarScanner
sonarscanner_version=4.7.0.2747-linux
sonarscanner_zip=sonar-scanner-cli-${sonarscanner_version}.zip
curl -LO "https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/${sonarscanner_zip}"
unzip ${sonarscanner_zip} -d /progs
rm ${sonarscanner_zip}
ln -s /progs/sonar-scanner-${sonarscanner_version}/bin/sonar-scanner /usr/local/bin/sonar-scanner
 
#Hadolint
hadolint_version=v2.9.1
simple_arch=arm64
if [ "$(dpkg --print-architecture)" = "amd64" ]
then
    simple_arch=x86_64
fi
curl -L "https://github.com/hadolint/hadolint/releases/download/${hadolint_version}/hadolint-Linux-${simple_arch}" -o /usr/bin/hadolint
chmod u+x /usr/bin/hadolint

#Trivy - Docker Scan
trivy_version=0.27.1
simple_arch=64bit
if [ "$(dpkg --print-architecture)" = "arm64" ]
then
    simple_arch=ARM64
fi
echo "Current architecture: ${simple_arch}"
curl -LO "https://github.com/aquasecurity/trivy/releases/download/v${trivy_version}/trivy_${trivy_version}_Linux-${simple_arch}.deb"
dpkg -i trivy_${trivy_version}_Linux-${simple_arch}.deb
rm trivy_${trivy_version}_Linux-${simple_arch}.deb


# Helm
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh

rm -rf /var/lib/apt/*
